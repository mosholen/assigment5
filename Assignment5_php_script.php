<?php
  header('Content-Type: text/html; charset=utf-8');
  error_reporting(E_ERROR | E_WARNING | E_PARSE);

  
  processDocument('SkierLogs.xml');


  function textContentFromNode($node) {
    $res = '';
    $children = $node->childNodes;
    for ($i = 0; $i < $children->length; $i++) {
      $child = $children->item($i);
      if ($child->nodeType == XML_TEXT_NODE) {
        $res .= $child->nodeValue;
      }
    }
    return $res;
  }
  

  function generateItemContent($item) {
    $res = '';
    $Clubs = NULL; (CREATOR)
    $Skiers = NULL;   (title)
    $Distance = array();(Title)



    $nodes = $item->getElementsByTagName('Clubs');
        if ($nodes->length > 0) {
      $Clubs = textContentFromNode($nodes->item(0));
    }

        $nodes = $item->getElementsByTagName('Skiers');
        if ($nodes->length > 0) {
      $Skiers = textContentFromNode($nodes->item(0));
    }

    $dista = $item->getElementsByTagName('Distance');
    foreach($dista as $log){
      $temp = array();
      $Distancenodes = $log->getElementsByTagName('Distance');
      foreach ($Distancenodes as $Dn) {
        array_push($temp, textContentFromNode($Dn));
      }
      array_push($Distance, $temp);
    }
    $res .= FormatItemContent($Clubs, $Skiers, $Distance, $Clubsnodes, $log);
    return $res;
  }
  


  function FormatItemContent($Clubs, $Skiers, $Distance, $log) {
    $res = '';
    if ($Clubs) {
      $res .= "<h2>$Clubs: $Name</h2>\n";
    }
      $i = 0;
    
    foreach ($Distance as $Distances) {
      $i++;
      if(count($Distance) > 1){
        $res .= "<h3>Distance $i</h3>";
      }
      $res .= "<ol>";
      foreach ($Distances as $Total) {
        $res .= "<li>$Total</li>\n";
      }
      $res .= "</ol>";
    }
    


    return $res;
  }
  

  function processDocument($docUrl) {
    $doc = new DOMDocument();
    if (!$doc->load($docUrl)) {
      $title = 'Operation failed';
      $body = '<p>Couldnt open XML-fil</p>';
    } else {


      $xpath = new DOMXpath($doc);
      $elements = $xpath->query("/SkierLogs/Clubs");
      if (!is_null($elements) && $elements->length > 0) {
        $title = "Ski-List";
        $body = "<h1>$title</h1>\n";


        foreach ($elements as $element) {
          $body .= generateItemContent($element);
        }

      } else {
        $title = 'Operation failed';
        $body = '<p>didnt find &lt;food&gt;-elements in XML-file</p>';
      }
    } 
echo <<<HTML
<html>
<head>
<title>$title</title>
</head>
<body>
$body
</body>
</html>  
HTML;
  }
?>