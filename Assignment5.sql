-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: 05. Nov, 2017 02:28 AM
-- Server-versjon: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Assignment5`
--

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `Season`
--

CREATE TABLE `Season` (
  `fallYear` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `Season`
--

INSERT INTO `Season` (`fallYear`) VALUES
(2015),
(2016),
(2017);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `SeasonDistance`
--

CREATE TABLE `SeasonDistance` (
  `userName` varchar(20) DEFAULT NULL,
  `clubID` varchar(20) DEFAULT NULL,
  `totalDistance` varchar(20) NOT NULL,
  `fallYear` int(4) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `SeasonDistance`
--

INSERT INTO `SeasonDistance` (`userName`, `clubID`, `totalDistance`, `fallYear`) VALUES
('erik_lien', 'lhmr-ski', '10.2km', 2015),
('andr_stee', 'skiklubben', '2.4mil', 2016),
('arne_anto', 'vindil', '22.4mil', 2016),
('ande_andr', 'asker-ski', '40.2mil', 2016);

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `SeasonRoster`
--

CREATE TABLE `SeasonRoster` (
  `fallYear` int(4) DEFAULT NULL,
  `userName` varchar(20) NOT NULL,
  `clubID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `SeasonRoster`
--

INSERT INTO `SeasonRoster` (`fallYear`, `userName`, `clubID`) VALUES
(2015, 'ande_andr', 'asker-ski'),
(2015, 'andr_stee', 'skiklubben'),
(2016, 'arne_anto', 'vindil'),
(2017, 'erik_lien', 'lhmr-ski');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `SkiClub`
--

CREATE TABLE `SkiClub` (
  `clubID` varchar(20) NOT NULL,
  `clubName` varchar(32) DEFAULT NULL,
  `city` varchar(32) DEFAULT NULL,
  `county` varchar(32) DEFAULT NULL,
  `location` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `SkiClub`
--

INSERT INTO `SkiClub` (`clubID`, `clubName`, `city`, `county`, `location`) VALUES
('asker-ski', 'Asker Skiklubb', 'Asker', 'Akershus', 'slemmestadveien.72'),
('lhmr-ski', 'Lillehammer Skiklubb', 'Lillehammer', 'Oppland', 'Bogstadveien.32'),
('skiklubben', 'Trondhjems Skiklubb', 'Trondheim', 'Trøndelag', 'Rosenborgveien.23'),
('vindil', 'Vind Idrettslag', 'Gjøvik', 'Oppland', 'Vestre Totenveg.523');

-- --------------------------------------------------------

--
-- Tabellstruktur for tabell `Skiers`
--

CREATE TABLE `Skiers` (
  `userName` varchar(20) NOT NULL,
  `firstName` varchar(32) DEFAULT NULL,
  `lastName` varchar(32) DEFAULT NULL,
  `yearOfBirth` int(4) DEFAULT NULL,
  `clubID` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dataark for tabell `Skiers`
--

INSERT INTO `Skiers` (`userName`, `firstName`, `lastName`, `yearOfBirth`, `clubID`) VALUES
('ande_andr', 'Anders', 'Andresen', 2004, 'skiklubben'),
('ande_r&#xF8;nn', 'Anders', 'R&#xF8;nning', 2001, 'lhmr-ski'),
('andr_stee', 'Andreas', 'Steen', 2001, 'asker-ski'),
('anna_n&#xE6', 'Anna', 'N&#xE6;ss', 2005, 'skiklubben'),
('arne_anto', 'Arne', 'Antonsen', 2005, 'skiklubben'),
('astr_amun', 'Astrid', 'Amundsen', 2001, 'lhmr-ski'),
('bent_h&#xE5;la', 'Bent', 'H&#xE5;land', 2009, 'asker-ski'),
('bent_svee', 'Bente', 'Sveen', 2003, 'asker-ski'),
('cami_erik', 'Camilla', 'Eriksen', 2015, 'vindil'),
('dani_hamm', 'Daniel', 'Hammer', 2000, 'lhmr-ski'),
('erik_lien', 'Erik', 'Lien', 2008, 'vindil'),
('erik_pete', 'Erik', 'Pettersen', 2002, 'vindil');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Season`
--
ALTER TABLE `Season`
  ADD PRIMARY KEY (`fallYear`);

--
-- Indexes for table `SeasonDistance`
--
ALTER TABLE `SeasonDistance`
  ADD PRIMARY KEY (`totalDistance`),
  ADD KEY `userName` (`userName`),
  ADD KEY `clubID` (`clubID`),
  ADD KEY `fallYear` (`fallYear`);

--
-- Indexes for table `SeasonRoster`
--
ALTER TABLE `SeasonRoster`
  ADD PRIMARY KEY (`userName`),
  ADD KEY `clubID` (`clubID`),
  ADD KEY `fallYear` (`fallYear`);

--
-- Indexes for table `SkiClub`
--
ALTER TABLE `SkiClub`
  ADD PRIMARY KEY (`clubID`);

--
-- Indexes for table `Skiers`
--
ALTER TABLE `Skiers`
  ADD PRIMARY KEY (`userName`),
  ADD KEY `clubID` (`clubID`);

--
-- Begrensninger for dumpede tabeller
--

--
-- Begrensninger for tabell `SeasonDistance`
--
ALTER TABLE `SeasonDistance`
  ADD CONSTRAINT `seasondistance_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `Skiers` (`userName`),
  ADD CONSTRAINT `seasondistance_ibfk_2` FOREIGN KEY (`clubID`) REFERENCES `SkiClub` (`clubID`),
  ADD CONSTRAINT `seasondistance_ibfk_3` FOREIGN KEY (`fallYear`) REFERENCES `Season` (`fallYear`);

--
-- Begrensninger for tabell `SeasonRoster`
--
ALTER TABLE `SeasonRoster`
  ADD CONSTRAINT `seasonroster_ibfk_1` FOREIGN KEY (`userName`) REFERENCES `Skiers` (`userName`),
  ADD CONSTRAINT `seasonroster_ibfk_2` FOREIGN KEY (`clubID`) REFERENCES `SkiClub` (`clubID`),
  ADD CONSTRAINT `seasonroster_ibfk_3` FOREIGN KEY (`fallYear`) REFERENCES `Season` (`fallYear`);

--
-- Begrensninger for tabell `Skiers`
--
ALTER TABLE `Skiers`
  ADD CONSTRAINT `skiers_ibfk_1` FOREIGN KEY (`clubID`) REFERENCES `SkiClub` (`clubID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
